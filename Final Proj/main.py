'''
@file      main.py
@brief     This file contains the main method used to institute the ball balancing table control system.
@details   This file contains the main method that initializes all objects used in the program, referencing 
           the drivers for the following: Encoders, Motors, Touchscreen, and Controller. The new driver, unique
           to the term project, was the controller driver.  A controller object was created using the hardware
           objects generated from the other hardware drivers, along with the specified interval and desired 
           steady state location atop the touchscreen. This driver controlled the flow of the program with its 
           "run()" method.  At the specified interval, current ball x/y position and table x/y rotation angles are obtained,
           the error signals are calculated, the sum of the contributions due to each state variable is computed 
           (xd*k1 + thetad*k2 + x*k3 + theta*k4), and the corresponding duty cycle is assigned to each motor.
           To accurately run this method at a consistent rate, we used utime.ticks_ms(). We found that one loop through our
           entire program took between 7-10 ms, so we specified our interval to be 20ms. Once a loop was completed, we 
           updated a "next_time" variable with the next time the run() method would occur and then compared the current time 
           to the next_time variable until it was time to run it.  This process would then perpetually repeat. 
          
           
@author    Hunter Brooks and Ryan Funchess
@date      March 4, 2021
'''
import pyb
from pyb import Pin
import utime
from EncoderDriver_TP import EncoderDriver
from MotorDriver_TP import MotorDriver
from position_TP import position
from controller_TP import controller

# Leveling table before attempting is necessary to set encoder to proper initial value

# Encoders Section:

# Create the pin objects used for interfacing with the encoder driver
pin_E1_CH1 = pyb.Pin(pyb.Pin.cpu.C6,pyb.Pin.IN)        # make E1_CH1 pin object
pin_E1_CH2 = pyb.Pin(pyb.Pin.cpu.C7,pyb.Pin.IN)        # make E1_CH2 pin object
pin_E2_CH1 = pyb.Pin(pyb.Pin.cpu.B6,pyb.Pin.IN)        # make E2_CH1 pin object
pin_E2_CH2 = pyb.Pin(pyb.Pin.cpu.B7,pyb.Pin.IN)        # make E2_CH2 pin object

# Create the timer objects used for encoder tick counting
timer1 = pyb.Timer(8, prescaler=0, period=0xFFFF)      # make timer 1 timer object
timer2 = pyb.Timer(4, prescaler=0, period=0xFFFF)      # make timer 2 timer object

# Create a motor object passing in the pins and timer
enc1 = EncoderDriver(pin_E1_CH1, pin_E1_CH2, timer1)   # make encoder 1 object
enc2 = EncoderDriver(pin_E2_CH1, pin_E2_CH2, timer2)   # make encoder 2 object

interval = 20 #Interval at which FSM will run tasks in ms
CPR = 1000    #CPR of motor

#Motors Section:

# Create the pin objects used for interfacing with the motor driver
pin_nSLEEP  = pyb.Pin(pyb.Pin.cpu.A15,pyb.Pin.OUT_PP)  # make nSLEEP pin object
pin_nFAULT  = pyb.Pin(pyb.Pin.cpu.B2,pyb.Pin.IN)       # make nFAULT pin object
pin_Button  = pyb.Pin(pyb.Pin.cpu.C13)                 # make the User Button pin object

pin_IN1     = pyb.Pin(pyb.Pin.cpu.B0,pyb.Pin.OUT_PP)   # make IN1 pin object
pin_IN2     = pyb.Pin(pyb.Pin.cpu.B1,pyb.Pin.OUT_PP)   # make IN2 pin object
pin_IN3     = pyb.Pin(pyb.Pin.cpu.B4,pyb.Pin.OUT_PP)   # make IN3 pin object
pin_IN4     = pyb.Pin(pyb.Pin.cpu.B5,pyb.Pin.OUT_PP)   # make IN4 pin object

# Create the timer object used for PWM generation
timer  = pyb.Timer(3,freq=20000)                       # create a timer object using timer 3, freq is 20kHz

# Create a motor object passing in the pins and timer
moe1     = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, timer, 1, pin_nFAULT, pin_Button) # make motor 1 object
moe2     = MotorDriver(pin_nSLEEP, pin_IN3, pin_IN4, timer, 2, pin_nFAULT, pin_Button) # make motor 2 object
   
# Enable the motor driver
moe1.enable()
moe2.enable()

#Touchpad
xm = Pin(Pin.cpu.A6)    # define a pin variable xm
xp = Pin(Pin.cpu.A0)    # define a pin variable xp
ym = Pin(Pin.cpu.A1)    # define a pin variable ym   
yp = Pin(Pin.cpu.A7)    # define a pin variable yp
w = 176                 # width of touch panel in units of mm, as specified by manufacturer
l = 99.36               # length of touch panel in units of mm, as specified by manufacturer
x_c = w/2               # x-distance from bottom left edge of touch panel to center, in units of mm
y_c = l/2               # y-distance from bottom left edge of touch panel to center, in units of mm

tscreen = position(xp,xm,yp,ym,w,l,x_c,y_c)  # create instance object "tst" for testing

#Closed loop control/controller
posref=[0,0]   # specify a reference position to represent desired steady state value

scalar = 40    # Scalar representing a multiplication factor for each of our gains
kx=[0.1*scalar,0.7*scalar,35*scalar,11*scalar]   # Gains applied to motor rotating in x-direction
ky=[0.1*scalar,-0.7*scalar,40*scalar,-11*scalar] # Gains applied to motor rotating in y-direction

## Iteration 1 - Balancing Table
# scalar = 70
# kx=[0,0.2227*scalar,0,7*scalar]
# ky=[0,-0.2227*scalar,0,-7*scalar]

## Iteration 2 - Ball + Platform, Ts too large and ess too small
# kx=[2*scalar,0.2227*scalar,8.5113*scalar,7*scalar]
# ky=[2*scalar,-0.2227*scalar,8.5113*scalar,-7*scalar]

## Gains before revisiting original model
# kx=[12*scalar,3*scalar,14*scalar,10*scalar]
# ky=[12*scalar,-3*scalar,35*scalar,-10*scalar]

## Values before Recalculation on 3/15/21
# scalar = 160
# kx=[3*scalar,0.45*scalar,17*scalar,14*scalar]
# ky=[6*scalar,0.45*scalar,17*scalar,20*scalar]

## Values from second Calc
# kx=[11.9619*scalar,1.6492*scalar,18.8531*scalar,22.5504*scalar]
# ky=[11.9619*scalar,-1.6492*scalar,18.8531*scalar,-22.5504*scalar]

## Balance Table
# kx=[0*scalar,3*scalar,0*scalar,6*scalar]
# ky=[0*scalar,-3*scalar,0*scalar,-6*scalar]

## These gains are alright
# scaler = 70
# kx=[2*scalar,0.34*scalar,5*scalar,4*scalar]
# ky=[2*scalar,-0.5*scalar,8*scalar,-4*scalar]

## Marginal Improvement
#scaler = 70
#kx=[1*scalar,0.6*scalar,18*scalar,4.5*scalar]
#ky=[1*scalar,-0.6*scalar,16*scalar,-4.5*scalar]

## Gains after correcting Motors
# scalar = 60
# kx=[3.5*scalar,1.5*scalar,20*scalar,20*scalar]
# ky=[3.5*scalar,-1.5*scalar,20*scalar,-20*scalar]

# Create controller object: gains, steady state location, motors, encoders, touchscreen, interval
controller=controller(kx,ky,posref,moe2,moe1,enc1,enc2,tscreen,interval)

while True:
    try:
        controller.run()
    except KeyboardInterrupt:  # When closing the program, the file prints the largest measured gain values during the trial
        moe1.disable()
        print('CTRL + C has been pressed, so motors have been disabled and the program ended')
        print('X-maxes below')
        print('G1:'+str(controller.G1xmax)) 
        print('G2:'+str(controller.G2xmax))
        print('G3:'+str(controller.G3xmax))
        print('G4:'+str(controller.G4xmax))
        print('Y-maxes below')
        print('G1:'+str(controller.G1ymax))
        print('G2:'+str(controller.G2ymax))
        print('G3:'+str(controller.G3ymax))
        print('G4:'+str(controller.G4ymax))
        break