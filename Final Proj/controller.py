'''
@file      controller.py
@brief     This file contains the controller class used to institute the ball balancing table control system.
@details   This file contains a controller object that acts as a closed loop system. The working principle of the object
is that it takes in inputs from the touchpad and encoders, determines the duty cycle to apply to each motor, and applies 
the duty cycle to each motor. Upon construction, a controller object is passed the relevant gain vectors, reference position,
motor, and encoder for each of the two degrees of freedom, as well as the relevant touchpad object and interval at which the run method
is to run. The run() method is run at the rate specified by interval in main.py.

@author    Hunter Brooks and Ryan Funchess
@date      March 15, 2021
'''

import pyb
import utime
import math
pi = math.pi

class controller:
    '''
    @brief  This Class implements a closed loop control algorithm
    '''
    Gate = True
    first_reading = True
    G1xmax=0
    G2xmax=0
    G3xmax=0
    G4xmax=0
    
    G1ymax=0
    G2ymax=0
    G3ymax=0
    G4ymax=0
    
    offsetx=20
    offsety=35
    sat_limit=85
    
    def __init__(self,kx,ky,posref,motx,moty,encx,ency,tscreen,interval):
        '''
        @brief This method constructs a controller object based on passed objects
        @param kx List of length 4 containing gains relevant to ball velocity, omega, ball position, and theta respectively for the x DOF
        @param ky List of length 4 containing gains relevant to ball velocity, omega, ball position, and theta respectively for the y DOF
        @param posref List of length 2 containing the x and y reference position for where the ball should rest
        @param motx Motor Driver object that controls the motor for the X-DOF
        @param moty Motor Driver object that controls the motor for the Y-DOF
        @param encx Encoder Driver object that monitors the angle of the motor for the X-DOF
        @param ency Encoder Driver object that monitors the angle of the motor for the Y-DOF
        @param tscreen Touch Screen Driver object that monitors x and y position of ball
        @param interval Integer that holds the desired time between FSM runs in ms
        
        '''        
        
        ##List of length 4 containing gains relevant to ball velocity, omega, ball position, and theta respectively for the x DOF
        self.kx=kx
        
        ##List of length 4 containing gains relevant to ball velocity, omega, ball position, and theta respectively for the y DOF
        self.ky=ky
        
        ##List of length 2 containing the x and y reference position for where the ball should rest
        self.posref=posref
        
        ##Motor Driver object that controls the motor for the X-DOF
        self.motx=motx
        
        ## Motor Driver object that controls the motor for the Y-DOF
        self.moty=moty
        
        ## Encoder Driver object that monitors the angle of the motor for the X-DOF
        self.encx=encx
        
        ## Encoder Driver object that monitors the angle of the motor for the Y-DOF
        self.ency=ency
        
        ## Touch Screen Driver object that monitors x and y position of ball
        self.tscreen=tscreen
        
        ## Integer that holds the desired time between FSM runs in ms
        self.interval=interval
        
        ## The integer timestamp for the first iteration
        self.start_time = utime.ticks_ms()
        # print('Start time:'+str(self.start_time))
        
        ## The integer "timestamp" for when the next run should be
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## The largest acceptable encoder angle in radians. If this is exceeded, the motors will be disabled.
        self.too_large = 40*(2*pi/360)
        
        ## Length of the lever arm in m
        self.rm=0.06
        
        ## Distance from table pivot to table push rod connection in m
        self.lp=0.11

        
    def run(self):
        '''
        @brief  This method runs the closed loop control system
        @ details This method is run in a While(true) loop in main.py. This method
        works cooperatively in that it first checks if it is time to run based on the passed interval, and passes otherwise.
        If the current time is equal to or past the denoted next time to run, the position and velocity of the ball as well as the 
        angle and angular velocity of the table are found in both degrees of freedom. These values are then stored in two vectors that contain
        all relevant information for each degree of freedom. These vectors are then "corrected" by subtracting our observed position
        by our desired position to obtain our error signal. The corrected vectors for each degree of freedom are then
        passed to a control method that computes the duty cycle to apply to the motor based on our numerical MATLAB analysis. These duty cycles
        are finally applied to each motor respectively and the next time for this method to run is specified based on the interval attribute.
        '''
        curr_time=utime.ticks_ms() #current time 
        if utime.ticks_diff(curr_time,self.next_time)>0: #if current time is past the designated next time
            positions = self.scan_positions() #obtain x, xdot, y, ydot
            angles = self.scan_angles()  #obtain theta x, theta y, theta dot x, theta dot y for the table
            x = [positions[1], angles[3], positions[0], angles[1]] #store xdot, theta ydot, x, theta in x, represents x DOF
            y = [positions[3], angles[2], positions[2], angles[0]] #store ydot, theta xdot, y, theta in y, represents y DOF
            
            x = self.correctDOF1(x) #subtract measured values from reference x position to get error signal
            y = self.correctDOF2(y) #subtract measured values from reference y position to get error signal
            Dx = self.controlDOF1(x) #obtain duty cycle to apply to x motor based on MATLAB analysis/tuning
            Dy = self.controlDOF2(y) #obtain duty cycle to apply to y motor based on MATLAB analysis/tuning
            
            self.motx.set_duty(Dx) #Apply output duty cycle to motor x
            self.moty.set_duty(Dy) #Apply output duty cycle to motor y
        
            self.next_time = utime.ticks_add(self.next_time, self.interval) #set the next time for the task to actually run
            
    def scan_positions(self): 
        '''
        @brief Handles ball kinematics (position and velocity of ball x and y directions)
        @details 

        '''
        ball_kinematics = self.tscreen.my_position()  # ball_kinematics is vector with length 5 containing [x,y,z,xd,yd]
        if ball_kinematics[2] == False:
            self.Gate = False
            self.motx.disable()
            
        elif ball_kinematics[2] == True:
		        self.motx.enable()
        else:
            pass

        x_current = ball_kinematics[0]
        delta_x_current = ball_kinematics[3]
        xd_current = delta_x_current/(self.interval/1000)
        
        y_current = ball_kinematics[1]
        delta_y_current = ball_kinematics[4]
        yd_current = delta_y_current/(self.interval/1000)
        
        if self.first_reading == True:
            self.first_reading = False
            xd_current = 0
            yd_current = 0

        return(x_current,xd_current,y_current,yd_current)
    
    def scan_angles(self):
        '''
        @brief This function acts as a getter for Kp.
        '''
      
        E_angle_x = self.encx.update_rad()   # positive x encoder angle is negative y table angle
        E_angle_y = self.ency.update_rad()    # positive y encoder angle is positive x table angle

        if E_angle_x > self.too_large  or E_angle_x < -1*(self.too_large) or E_angle_y > self.too_large  or E_angle_y < -1*(self.too_large):
           self.motx.disable() 
        else:
	        pass
        Table_x_angle = (self.rm/self.lp)*E_angle_y
        Table_y_angle = -(self.rm/self.lp)*E_angle_x

        E_delta_x = self.encx.get_delta_rad()   # positive x encoder vel is negative y table vel
        E_vel_x =  E_delta_x/(self.interval/1000)                 # MUST UPDATE interval here with actual variable
        E_delta_y = self.ency.get_delta_rad()    # positive y encoder vel is positive x table vel
        E_vel_y =  E_delta_y/(self.interval/1000)                 # MUST UPDATE interval here with actual variable

        Table_x_vel = (self.rm/self.lp)*E_vel_y
        Table_y_vel = -(self.rm/self.lp)*E_vel_x

        return(Table_x_angle, Table_y_angle, Table_x_vel, Table_y_vel)

    def correctDOF1(self,x):
        '''
        @brief Correct the measurements taken to obtain an error signal for the x DOF
        @details This method subtracts the reference position from measured position in order to 
        obtain an error signal upon which the control voltage can be sourced. Since the reference position for
        the ball is the middle of the table, posref[0] is 0 for this balacning situation.
        @param x List containing xdot, theta dot, x, theta relevant to x direction
        @return x List containing error xdot, theta dot, x, theta relevant to x direction
        '''
        x[2]=x[2]-self.posref[0] #obtain error signal by substracting reference ball position from measured x
        
        return x
    
    def correctDOF2(self,y):
        '''
        @brief Correct the measurements taken to obtain an error signal for the y DOF
        @details This method subtracts the reference position from measured position in order to 
        obtain an error signal upon which the control voltage can be sourced. Since the reference position for
        the ball is the middle of the table, posref[1] is 0 for this balacning situation.
        @param y List containing ydot, theta dot, y, theta relevant to y direction
        @return y List containing error ydot, theta dot, y, theta relevant to y direction
        '''
        y[2]=y[2]-self.posref[1] #obtain error signal by substracting reference ball position from measured y
        return y
    
    def controlDOF1(self,x):
        '''
        @brief Obtain 
        @param
        @return D
        
        '''
        #Put angles in RADIANS
        G1=x[0]*self.kx[0]
        G2=x[1]*self.kx[1]
        G3=x[2]*self.kx[2]
        G4=x[3]*self.kx[3]
        if self.Gate == True:
            if(abs(G1)>self.G1xmax):
                self.G1xmax=abs(G1)
            if(abs(G2)>self.G2xmax):
                self.G2xmax=abs(G2)
            if(abs(G3)>self.G3xmax):
                self.G3xmax=abs(G3)
            if(abs(G4)>self.G4xmax):
                self.G4xmax=abs(G4)
            
        D=G1+G2+G3+G4
        if D==0:
            return D     
        elif(abs(D)<offsetx)
            D=offsetx*D/abs(D)
        else:
            choice=[abs(D),self.sat_limit]
            return (min(choice)*D/abs(D))   
        
    def controlDOF2(self,y):
        #PUT ANGLES IN RADIANS        
        G1=y[0]*self.ky[0]
        G2=y[1]*self.ky[1]
        G3=y[2]*self.ky[2]
        G4=y[3]*self.ky[3]
        
        if self.Gate == True:
            if(abs(G1)>self.G1ymax):
                self.G1ymax=abs(G1)
            if(abs(G2)>self.G2ymax):
                self.G2ymax=abs(G2)
            if(abs(G3)>self.G3ymax):
                self.G3ymax=abs(G3)
            if(abs(G4)>self.G4ymax):
                self.G4ymax=abs(G4)  
       
        D=G1+G2+G3+G4
        if D==0:
            return D     
        elif(abs(D)<offsety)
            D=offsety*D/abs(D)
        else:
            choice=[abs(D),self.sat_limit]
            return (min(choice)*D/abs(D))
            
    
        
                
        