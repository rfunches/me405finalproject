'''
@file      position_TP.py
@brief     This file contains a hardware driver to interface a resistive touch panel with the STM 32 microcontroller.
@details   The class for this driver is named position and it contains a constructor that takes in eight total inputs, 
           including: four arbitrary pins (xp,xm,yp,and ym), the width (x-dimension) and length (y-dimension) of the
           touch panel, and the x and y distances from the bottom left corner of the touch panel to the center of the 
           touch panel.  The driver contains the following modules: "x_coord" - measures and returns the horizontal 
           position in units of mm of a point of contact from the center of the touch panel; "y_coord" - measures and
           returns the vertical position in units of mm of a point of contact from the center of the touch panel; 
           "z_coord" - measures and returns boolean True if there is contact with the touch panel or False if no 
           contact is sensed; "my_position" - runs "x_coord", "y_coord", and "z_coord" and returns a tuple containing 
           their outputs in that order. The measurement the functions receive are voltage divider ratios from one of 
           two potentiometers (one measuring x direction and one measuring y direction).  This reading is normalized 
           to the specified overall length and width of the touch panel and then offset according to the specified 
           distance to the center of the touch panel. The resistive touch panel this driver was designed to interface 
           with will be used to indicate the planar position of the ball on the balance platform for this course's 
           final project.
@author    Hunter Brooks
@date      March 4, 2021
'''
# The following two lines of code are used to receive an error message if an error occurs when executing the program.
import micropython
micropython.alloc_emergency_exception_buf(200)  # the buffer contains 200 bytes of memory

# Import the necessary modules for this driver
import pyb
from pyb import Pin
from pyb import ADC
import utime

class position:
    '''
    @brief    This class is a driver for the resistive touch panel
    @details  This class allows you to measure the location of a contact point on the 
              resistive touch panel.  To do so, the four arbitrary pins(xm,xp,ym,and yp)
              the dimensions of the touch panel (width and length) and the x and y 
              coordinates of its center must be specified in the constructor
    '''              

    # Class Variables:
        # Calibration ADC Limits - These were used to calibrate the x_coord and y_coord
        # modules. They represent the measuring limits in the x and y directions since the
        # touch panel is not capable of measuring the entire area of the panel (width x length)
    
    ## @brief   this is the minimum readable measurement in the x direction of the touch panel
    #  @details when I ran my finger across the length of the touch panel, this was the value being
    #           output when I reached the left edge. This is used in x_coord() to account for the reduced
    #           reading span in the x direction
    x_min = 210
    
    ## @brief   this is the maximum readable measurement in the x direction of the touch panel
    #  @details when I ran my finger across the length of the touch panel, this was the value being
    #           output when I reached the right edge. This is used in x_coord() to account for the reduced
    #           reading span in the x direction
    x_max = 3794
    
    ## @brief   this is the minimum readable measurement in the y direction of the touch panel
    #  @details when I ran my finger across the length of the touch panel, this was the value being
    #           output when I reached the bottom edge. This is used in y_coord() to account for the reduced
    #           reading span in the y direction
    y_min = 375
    
    ## @brief   this is the maximum readable measurement in the x direction of the touch panel
    #  @details when I ran my finger across the length of the touch panel, this was the value being
    #           output when I reached the top edge. This is used in y_coord() to account for the reduced
    #           reading span in the y direction
    y_max = 3549
    
    def __init__(self,xp,xm,yp,ym,w,l,x_c,y_c):
        '''
        @brief         initializes the "position" touch panel driver and assigns the desired attributes
                       to the object for the touch panel - STM 32 mcu interface
        @param PIN.xp  positive x pin that will be manipulated in the following modules
        @param PIN.xm  minus x pin that will be manipulated in the following modules
        @param PIN.yp  positive y pin that will be manipulated in the following modules
        @param PIN.ym  minus y pin that will be manipulated in the following modules
        @param w       Width of the touch panel as specified in the documenation from manufacturer
        @param l       Length of the touch panel as specified in the documenation from manufacturer
        @param x_c     X-distance from the bottom left corner of the touch panel to its center
        @param y_c     Y-distance from the bottom left corner of the touch panel to its center
        '''
        self.PIN_xp = xp
        self.PIN_xm = xm
        self.PIN_yp = yp
        self.PIN_ym = ym
        self.w      = w
        self.l      = l
        self.x_c    = x_c
        self.y_c    = y_c
        self.my_x = 0
        self.my_y = 0

    def x_coord (self):
        '''
        @brief          This method reads the horizontal potentiometer and outputs an x-distance from center for a contact point
        @details        This method initializes pins and sets them to input/output, high/low, and analog mode. After creating
                        an ADC variable, the program reads the sensor and (after a delay) converts it to a distance in mm 
                        relative to the center of the touch panel.
        '''
        self.PIN_xm.init(mode=Pin.OUT_PP, value=1)  # Initialize pin xm, configure for output and set to low
        self.PIN_xp.init(mode=Pin.OUT_PP, value=0)  # Initialize pin xp, configure for output and set to high
        self.PIN_ym.init(mode=Pin.IN)               # Initialize pin ym, configure for input
        self.PIN_yp.init(mode=Pin.IN)               # Initialize pin yp, configure for input
        self.PIN_ym.init(mode=Pin.ANALOG)           # Initialize pin ym, configure for analog
        self.ADC_ym = ADC(self.PIN_ym)              # Create an analog object for pin ym
        pyb.udelay(4)                               # Delay 4 microseconds to settle resistor dividers before reading ADC channel
        self.x_val = (self.ADC_ym.read()-position.x_min)/(position.x_max-position.x_min)*self.w - self.x_c # convert to mm relative to x_c
        return self.x_val                           # Return the calculated value from previous line
        
    def y_coord (self):    
        '''
        @brief          This method reads the vertical potentiometer and outputs a y-distance from center for a contact point
        @details        This method initializes pins and sets them to input/output, high/low, and analog mode. After creating
                        an ADC variable, the program reads the sensor and (after a delay) converts it to a distance in mm 
                        relative to the center of the touch panel.
        '''                        
        self.PIN_ym.init(mode=Pin.OUT_PP, value=0)  # Initialize pin ym, configure for output and set to high
        self.PIN_yp.init(mode=Pin.OUT_PP, value=1)  # Initialize pin yp, configure for output and set to low
        self.PIN_xm.init(mode=Pin.IN)               # Initialize pin xm, configure for input
        self.PIN_xp.init(mode=Pin.IN)               # Initialize pin xp, configure for input
        self.PIN_xm.init(mode=Pin.ANALOG)           # Initialize pin xm, configure for analog
        self.ADC_xm = ADC(self.PIN_xm)              # Create an analog object for pin xm
        pyb.udelay(4)                               # Delay 4 microseconds to settle resistor dividers before reading ADC channel
        self.y_val = (self.ADC_xm.read()-position.y_min)/(position.y_max-position.y_min)*self.l - self.y_c # convert to mm relative to y_c
        return self.y_val                           # Return the calculated value from previous line
        
    def z_coord (self): 
        '''
        @brief          This method reads the voltage at the center node when both resistor dividers are energized and outputs a boolean representing if panel is being touched
        @details        This method initializes pins and sets them to input/output, high/low, and analog mode. After creating
                        an ADC variable, the program reads the sensor and (after a delay) determines if the panel is being touched
        '''    
        self.PIN_xm.init(mode=Pin.OUT_PP, value=0)  # Initialize pin xm, configure for output and set to low
        self.PIN_yp.init(mode=Pin.OUT_PP, value=1)  # Initialize pin yp, configure for output and set to high
        self.PIN_ym.init(mode=Pin.IN)               # Initialize pin ym, configure for input
        self.PIN_xp.init(mode=Pin.IN)               # Initialize pin xp, configure for input
        self.PIN_ym.init(mode=Pin.ANALOG)           # Initialize pin ym, configure for analog
        self.ADC_ym = ADC(self.PIN_ym)              # Create an analog object for pin ym 
        pyb.udelay(4)                               # Delay 4 microseconds to settle resistor dividers before reading ADC channel
        self.z = self.ADC_ym.read()                 # Read an analog value from ADC object
        if self.z <= 4000:                          # Since reading on pin ym, if reading is approx. 4095 then no contact will be sensed
            self.z_val = True                       #     To account for some fluctuation, I compare to 4000 and set z_val to boolean accordingly 
        else:
            self.z_val = False
        return self.z_val
        
    def my_position(self):
        '''
        @brief          This method calls methods x_coord(),y_coord(),and z_coord(), as well as change in position in the x and y, and returns a tuple.
        @return my_cords Tuple containing x position, y position, z position, x delta, and y delta respectively in m
        '''   
        self.last_x = self.my_x 
        self.last_y = self.my_y 

        self.my_x = self.x_coord()                       # call x_coord() and store its output as self.my_x
        self.my_y = self.y_coord()                       # call y_coord() and store its output as self.my_y
        self.my_z = self.z_coord()                       # call z_coord() and store its output as self.my_z
        
        self.delta_x = self.my_x - self.last_x            #change in x position
        self.delta_y = self.my_y - self.last_y            #change in y position
        
        self.my_coords = (self.my_x/1000,self.my_y/1000,self.my_z,self.delta_x/1000,self.delta_y/1000) # create tuple from results of called functions
        
        return self.my_coords                            # return the tuple
        
    
# Test code for testing my_position() and channel timing, including the pin mapping, w, l, x_c, and y_c values I used when testing
if __name__ == "__main__":
    xm = Pin(Pin.cpu.A6)    # define a pin variable xm
    xp = Pin(Pin.cpu.A0)    # define a pin variable xp
    ym = Pin(Pin.cpu.A1)    # define a pin variable ym   
    yp = Pin(Pin.cpu.A7)    # define a pin variable yp
    w = 176                 # width of touch panel in units of mm, as specified by manufacturer
    l = 99.36               # length of touch panel in units of mm, as specified by manufacturer
    x_c = w/2               # x-distance from bottom left edge of touch panel to center, in units of mm
    y_c = l/2               # y-distance from bottom left edge of touch panel to center, in units of mm
    
    tst = position(xp,xm,yp,ym,w,l,x_c,y_c)  # create instance object "tst" for testing
    begin = utime.ticks_us()                 # take time stamp before reading timers
    current_position = tst.my_position()     # call my_position and store values in current_time
    end = utime.ticks_us()                   # take time stamp after reading timers
    timing = utime.ticks_diff(end,begin)     # determine timing by calculating difference of time stamps
    print(current_position)                  # output function output
    print('All three channels were read and conversions were executed in '+ str(timing) + ' microseconds')
    
    