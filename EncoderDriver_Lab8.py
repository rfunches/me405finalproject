'''
@file      EncoderDriver.py
@brief     This file contains an encoder driver that continually returns the updated encoder angle.
@details   This driver contains five methods that operate cohesively to continuously calculate the angle of the encoder.  
           These methods include: (1): __init__() - initializes encoder objects, (2): update() - updates the calculated angular
           position of the encoder, (3): get_position() - returns the most recently updated encoder position, (4): set_position() - 
           resets the position of the encoder to a specified value, (5): get_delta - returns the difference in recorded position 
           between the two most recent position measurements. This file takes pin and timer objects, and configures the timer objects
           to encoder mode, with a counter that changes when either CH1 or CH2 changes. When either channel changes by one logic level,
           the timer reads one tick of motion. Since there are 4000 ticks per encoder revoluation, we can use this measurement of ticks to 
           calulate the angular position at any given moment. To calculate the current position of the encoder (returned by update()), 
           we work in units of ticks, then convert at this end to angle.  The current tick value is measured from the encoder and the tick
           value from the previous measurement is subtracted from it.  The result of this operation is our raw delta ticks value. 
           The raw delta ticks value is then compared to the acceptable range of ticks delta ticks (-period/2 to period/2, where period = 65535).
           If acceptable, the delta ticks is then converted to units of angles by the fact that there are 4000 ticks per 360 degrees. This value
           is used by the to the previous anglular position value, returned from the get_position() function, in the update() function. 
           This value is then kept in the get_position() function for the next iteration.
@author    Hunter Brooks and Ryan Funchess
@date      March 8, 2021
'''

# Import the necessary module for this driver.
import pyb
# The following two lines of code are used to receive an error message if an error occurs when executing the program.
import micropython
micropython.alloc_emergency_exception_buf(200)  # the buffer contains 200 bytes of memory

class EncoderDriver:
    ''' 
    @brief   This class implements an encoder driver for the ME405 board which continually returns the updated encoder angle. 
    @details This driver contains five methods that operate cohesively to continuously calculate the angle of the encoder.  
             These methods include: (1): __init__() - initializes encoder objects, (2): update() - updates the calculated angular
             position of the encoder, (3): get_position() - returns the most recently updated encoder position, (4): set_position() - 
             resets the position of the encoder to a specified value, (5): get_delta - returns the difference in recorded position 
             between the two most recent position measurements. This file takes pin and timer objects, and configures the timer objects
             to encoder mode, with a counter that changes when either CH1 or CH2 changes.
    '''
    
    # Class Variable:
    
    ## @brief   this is the magnitude of the timer period and is used to check for timer underflow or overflow
    #  @details this value is used to test if a delta ticks value is acceptable. if not, underflow or 
    #           overflow occured. 
    period = 65535
    
    CPR=1000
    
    def __init__ (self, pin_E_CH1, pin_E_CH2, timer):
         '''
         @brief Creates an encoder driver by initializing pins and timers.
         @param pin_E_CH1      A pyb.Pin object to use as the channel 1 pin.
         @param pin_E_CH2      A pyb.Pin object to use as the channel 2 pin.
         @param timer          A pyb.Pin object to use as the ticks counter.
         '''
         
         self.pin_E_CH1      = pin_E_CH1
         self.pin_E_CH2      = pin_E_CH2
         self.timer          = timer
         self.timer.channel(1,pyb.Timer.ENC_AB,pin=self.pin_E_CH1) # configure timer channel 1 to encoder mode, with counter changes when CH1 or CH2 changes 
         self.timer.channel(2,pyb.Timer.ENC_AB,pin=self.pin_E_CH2) # configure timer channel 2 to encoder mode, with counter changes when CH1 or CH2 changes 
         
         self.current_count    = 0    # initialize initial count to 0
         self.current_position = 0    # initialize initial angular position to zero
         print('Creating an encoder driver')
        
    def update(self):
        '''
        @brief Updates and returns the recorded position of the encoder. This is done by calling delta_position() function to calculate the
               most recent change in encoder angle in degrees, calling get_position() function to return the previous angular position and adding
               the two values.
        '''
        self.delta_position = self.get_delta()      # calculate change in angular position from previous two encoder readings
        self.last_position  = self.get_position()   # grab position value from previous reading
        self.current_position = self.last_position + self.delta_position  # calculate the current angular position by adding 
        return self.current_position                                      #   the previous position to the most recent change in angular position
        
    def get_position(self):
        '''
        @brief Returns the most recently updated position of the encoder. This is either from the update() or user specified angular position.
        '''
        
        return self.current_position
        
    def set_position(self,updated_position):
        ''' 
        @brief Resets the angular position of the encoder to a specified value in degrees.
        '''
        self.updated_position = updated_position
        self.current_position = self.updated_position
        return self.current_position
        
    def get_delta (self):
        ''' 
        @brief Calculates and returns the difference in position between the two most recent position measurements.
        ''' 
        self.last_count = self.current_count      # store previous measured tick count in last_count
        self.current_count = self.timer.counter() # measure current tick count and store in current_count
        
        self.raw_delta_count = self.current_count - self.last_count  # raw_delta_count = change in ticks from two most recent readings
        
        if self.raw_delta_count >= -self.period/2 and self.raw_delta_count <= self.period/2:  # if delta count is between +/- period/2, its an 
            self.delta_count = self.raw_delta_count                                           #    acceptable delta
            
        elif self.raw_delta_count < -self.period/2:   # Correct for counter overflow by adding period to most recent ticks count
            self.current_count = self.period + self.current_count
            self.delta_count = self.current_count - self.last_count
            
        else:                                         # Correct for counter underflow by subtracting period from most recent ticks count
            self.current_count = self.current_count - self.period
            self.delta_count = self.current_count - self.last_count
            
        self.delta_position = self.delta_count # convert from ticks to angular position in degrees by multiplying by (360/4000)
        return self.delta_position 
    
    def update_rad(self):
        pos_ticks=self.update()
        pos_rad=pos_ticks/(4*self.CPR)*(2*3.1415) #convert from ticks to rad
        return pos_rad
    
    def get_delta_rad(self):
        delta_ticks=self.delta_position
        delta_rad=delta_ticks/(4*self.CPR)*(2*3.1415) #convert from ticks to rad
        return delta_rad
        
        
if __name__ =='__main__':
    # The following code represents a test program for the motor class.
    # Any code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.
    
    # Create the pin objects used for interfacing with the encoder driver
    pin_E1_CH1 = pyb.Pin(pyb.Pin.cpu.B6,pyb.Pin.IN)   # make E1_CH1 pin object
    pin_E1_CH2 = pyb.Pin(pyb.Pin.cpu.B7,pyb.Pin.IN)   # make E1_CH2 pin object
    
    pin_E2_CH1 = pyb.Pin(pyb.Pin.cpu.C6,pyb.Pin.IN)   # make E2_CH1 pin object
    pin_E2_CH2 = pyb.Pin(pyb.Pin.cpu.C7,pyb.Pin.IN)   # make E2_CH2 pin object
    
    # Create the timer objects used for encoder tick counting
    timer1 = pyb.Timer(4, prescaler=0, period=0xFFFF)  # make timer1 timer object
    timer2 = pyb.Timer(8, prescaler=0, period=0xFFFF)  # make timer2 timer object
    
    # Create a motor object passing in the pins and timer
    enc1 = EncoderDriver(pin_E1_CH1, pin_E1_CH2, timer1)
    enc2 = EncoderDriver(pin_E2_CH1, pin_E2_CH2, timer2)
    
    while True:
        print('enc1 [degrees] = ' + str(enc1.update()) + ' enc2 [degrees] = ' + str(enc2.update()))
        pyb.delay(100)